import './App.css';
import React, {useEffect, useState} from 'react';
import api from './api';

function App() {
  const [actualGroup, setActualGroup] = useState(null);
  const [admGroup, setAdmGroup] = useState(['Administrador', 'Grupo Teste']);
  const [msgList, setMsgList] = useState(['teste1', 'teste2']);
  const [admMessageList, setAdmMessageList] = useState(['admMessage1', 'admMessage2']);
  const [testMessageList, setTestMessageList] = useState(['msgTest1', 'msgTest2']);
  const [msg, setMsg] = useState('');

  useEffect(() => {       //Receber do servidor msgs e grupos
    (async function get(){ 
      await api.get('/').then((response) => {
        setMsgList(response.data);
        setActualGroup('Administrador');
        console.log(response);
      }).catch((error)=>{
        console.log(error);
      });
    })();
  }, []);

  useEffect(()=>{   
    
      const inputEle = document.getElementById('text-submit');
      inputEle.addEventListener('keyup', function(e){
        var key = e.which || e.keyCode;
        if (key == 13) { // codigo da tecla enter
          // colocas aqui a tua função a rodar
          sendToChat(this.value);
        }
      });
     
  
  },[]);
  
  const getMessages = (item) => {
    setActualGroup(item);
    if(admGroup == 'Administrador')
      setMsgList(admMessageList);
    else  
      setMsgList(testMessageList);
  }

  const sendToChat = (message) =>{
    if(message){
      setMsgList(msgList => [...msgList, message]);
      setMsg('');

    }
  }
    
  return (
    <div className="App">
      <main className='container'>
        
        <aside className= 'contacts'>
          <div className='group-card'>
            {
              admGroup ?
                admGroup.map((item, index) => (
                  <div 
                    className='group-item' 
                    key={index}
                    onClick={()=>{
                      getMessages(item)
                    }}
                  >  
                  {item}</div>
                ))
              : null
            }
          </div>
        </aside>

        {
          
            <div className='chat'>
              <header>
                <p>{actualGroup}</p>
              </header>

              <div className='chat-container'>
                {
                  msgList ?
                    msgList.map((item,index) => (
                      <div className='chat-item' key={index}>{item}</div>    
                    ))
                  : null
                }
              </div>
              <input 
                id='text-submit'
                className ='input' 
                type="text"
                onSubmit={sendToChat}
                value={msg}
                onChange={(e)=>{
                  setMsg(e.target.value)
                }}
              />
            </div>
          
        }
        
      </main>
    </div>
  );
}

export default App;


